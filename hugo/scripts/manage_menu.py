#!/usr/bin/python
import sys
# sys.setdefaultencoding() does not exist, here!
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')


import argparse
import sys
import pytoml
import os

HUGOPATH=os.path.realpath(__file__+"/../..")

def write_config(config):
  with open(os.path.join(HUGOPATH,'config.toml'), "w+") as conffile:
    pytoml.dump(conffile, config)

parser = argparse.ArgumentParser(description='Manage hugo menu')
parser.add_argument('--delete', action="store_true")
parser.add_argument('--add', action="store_true")
parser.add_argument('--name')
parser.add_argument('--url')
parser.add_argument('--id')
parser.add_argument('--weight', default=2)

args = parser.parse_args()

if not args.delete and not args.add:
  print "Flag delete or add needed"
  sys.exit(1)

if args.delete and args.add:
  print "Delete and add can't be used at the same time"

if args.add and (not args.name or not args.url):
  print "name and url are needed for add"
  sys.exit(1)

if not args.id:
  print "discourse ID is needed"
  sys.exit(1)

with open(os.path.join(HUGOPATH,'config.toml')) as conffile:
  config = pytoml.loads(conffile.read())

for m in config['menu']['main']:
  if m.has_key('discourse_id') and m['discourse_id'] == args.id:
    if args.add:
      # update the section if needed
      if m[u'url'] != args.url or m[u'name'] != args.name or m[u'weight'] != args.weight:
        m[u'url'] = args.url
        m[u'name'] = args.name
        m[u'weight'] = args.weight
        write_config(config)
    if args.delete:
      config['menu']['main'].remove(m)
      write_config(config)
    sys.exit(0)

m={
u'url': args.url,
u'discourse_id': args.id,
u'name': args.name,
u'weight': args.weight
}
config['menu']['main'].append(m)

write_config(config)
