#!/bin/bash

content_type=$(basename $0|cut -d '-' -f1)
type=$1
action=$2
hugo_dir=$(realpath $(dirname $0)/../)

if [ "$type" = "topic" ]
then
  slug=$3
  discourse_id=$4
  date=$5
  title=$6
  if [ -z "$action" ] || [ -z "$slug" ] || [ -z "$title" ] || [ -z "$discourse_id" ]
  then
    exit 1
  fi
elif [ "$type" = "post" ]
then
  slug=$3
  discourse_id=$4
  date=$5
  content=$6
  if [ -z "$action" ] || [ -z "$slug" ] || [ -z "$content" ] || [ -z "$discourse_id" ]
  then
    exit 1
  fi
else
  exit 1
fi

# handle html entities
title=$(echo $title| php -r 'while(($line=fgets(STDIN)) !== FALSE) echo html_entity_decode($line, ENT_QUOTES|ENT_HTML401);')

if [ "$action" = "topic_destroyed" ]
then
  if [ -e "$hugo_dir/content/${content_type}/${discourse_id}.html" ]
  then
    rm $hugo_dir/content/${content_type}/${discourse_id}.html
  fi
  if [ "$content_type" == "page" ]
  then
    $hugo_dir/scripts/manage_menu.py --delete --id "$discourse_id"
  fi
  exit 0
fi

if [ "$type" = "post" ]
then
  if [ -e "$hugo_dir/content/${content_type}/${discourse_id}.html" ]
  then
    title=$(grep -m1 '^title=' "$hugo_dir/content/${content_type}/${discourse_id}.html" | cut -d '=' -f '2'|cut -d '"' -f2)
  fi

cat << EOF > "$hugo_dir/content/${content_type}/${discourse_id}.html"
+++
title="$title"
draft = false
slug="$slug"
date="$date"
discourse_id="$discourse_id"
+++

$content
EOF
chmod 644 "$hugo_dir/content/${content_type}/${discourse_id}.html"
elif [ "$type" = "topic" ]
then
  if [ -e "$hugo_dir/content/${content_type}/${discourse_id}.html"  ]
  then
    # we can't do inplace sed because it creates temporary file that can be picked up by hugo
    tmpfile=$(mktemp)
    chmod 644 $tmpfile
    sed "0,/RE/s/title=.*/title=\"$title\"/" "$hugo_dir/content/${content_type}/${discourse_id}.html" > $tmpfile
    if [ $? -eq 0 ]
    then
      mv $tmpfile "$hugo_dir/content/${content_type}/${discourse_id}.html"
    else
      rm $tmpfile
      exit 1
    fi
  else
cat << EOF > "$hugo_dir/content/${content_type}/${discourse_id}.html"
+++
title="$title"
draft = true
slug="$slug"
date="$date"
discourse_id="$discourse_id"
+++

EOF
  chmod 644 "$hugo_dir/content/${content_type}/${discourse_id}.html"
  fi
  # update menu
  if [ "$content_type" == "page" ]
  then
    $hugo_dir/scripts/manage_menu.py --add --id "$discourse_id" --url "/${content_type}/$slug/" --name "$title"
  fi
fi

