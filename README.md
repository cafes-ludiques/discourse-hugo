## Discourse to hugo integration

This configuration allows to create a static site with hugo using discourse as the backend. You will need:

* [Hugo](https://gohugo.io/)
* [webhook](https://github.com/adnanh/webhook/)
* [Discourse](https://www.discourse.org/)

### Configure Hugo

The hugo directory contains some files needed in your hugo configuration. The most important is the scripts directory, which contain scripts to create/remove page and post, and a script to update the hugo menu. The archetypes directory contain the configuration needed in order to add the discourse ID of the posts in the hugo content files.

You don't need the layouts directory but you must get the section {{ if.Site.Params.DiscourseUrl }} and add it to your post.html in order to have the comment to discourse feature. Same for static dir, you only need the css configuration for discourse.

The hugo.service is an example systemd configuration file for Hugo.

## Configure webhook

You can use the webhook.conf file for configuring webhook. Just change the path to your hugo site.

You wil also need to have your webserver proxy the /hooks path to webhook. In apache you can do something like:

```
ProxyRequests Off
ProxyPass          /hooks/ http://127.0.0.1:9000/hooks/
<Location          /hooks/>
require ip xx.xx.xx.xx # public IP of discourse server
ProxyPassReverse http://127.0.0.1:9000/hooks/
</Location>
```

## Configure discourse

### Discourse webhooks

In Administration -> API -> Webhooks, you will need to create 4 webhooks:

#### Post update topic

* Payload URL: https://example.com/hooks/post-update-topic
* Content type: application/json
* Which events: Topic Event
* Triggered Categories: select which categories will create posts on the static site
* Don't forget to check active (and Check TLS cert)

#### Post update post
* Payload URL: https://example.com/hooks/post-update-post
* Content type: application/json
* Which events: Post Event
* Triggered Categories: select which categories will create posts on the static site (same as Post update topic)
* Don't forget to check active (and Check TLS cert)

#### Page update topic

* Payload URL: https://example.com/hooks/page-update-topic
* Content type: application/json
* Which events: Topic Event
* Triggered Categories: select which categories will create pages on the static site
* Don't forget to check active (and Check TLS cert)

#### Page update post
* Payload URL: https://example.com/hooks/page-update-post
* Content type: application/json
* Which events: Post Event
* Triggered Categories: select which categories will create pages on the static site (same as Page update topic)
* Don't forget to check active (and Check TLS cert)

### Discourse embedding

In Administration -> Customize -> Embedding add a new Host. Fill with something like:

* Allowed Hosts: example.com
* Path Whitelist: /post/.*
* Post to Category: same as Post webhooks categorie
